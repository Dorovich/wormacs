(use-package zenburn-theme
  :ensure t
  :defer t)

(use-package hc-zenburn-theme
  :ensure t
  :defer t)

(use-package poet-theme
  :ensure t
  :defer t)

(use-package ef-themes
  :ensure t
  :defer t)
